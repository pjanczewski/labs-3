﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    public class Roomba
    {
        public static ISterowanie sterowanie;
        private static IPamiec pamiec;
        private static IZasilanie zasilanie;

        public static object DostepDoSterowania(object impl)
        {
            if (sterowanie == null)
                sterowanie = new Sterowanie();
            return sterowanie;
        }

        public static object DostepDoPamieci(object impl)
        {
            if (pamiec == null)
                pamiec = new Pamiec();
            return pamiec;
        }

        public static object DostepDoZasilania(object impl)
        {
            if (zasilanie == null)
                zasilanie = new Zasilanie();
            return zasilanie;
        }
    }
}
