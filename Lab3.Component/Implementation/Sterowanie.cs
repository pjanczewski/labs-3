﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class Sterowanie : ISterowanie
    {
        public void SkrecWPrawo()
        {
            Console.WriteLine("Skręć w prawo");
        }

        public void SkrecWLewo()
        {
            Console.WriteLine("Skręć w lewo");
        }

        public void JedzProsto()
        {
            Console.WriteLine("Nie skręcaj");
        }
    }
}
