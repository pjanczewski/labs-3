﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class Zasilanie : IZasilanie
    {
        private bool wlaczony = false;
        public void Wlacz()
        {
            wlaczony = true;
        }

        public void Wylacz()
        {
            wlaczony = false;
        }
    }
}

