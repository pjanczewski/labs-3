﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;

namespace Lab3.Implementation
{
    class Pamiec :IPamiec
    {
        private double akumulator = 0;
        public void Zapisz(double a)
        {
            akumulator += a;
        }

        public double Odczytaj()
        {
            return akumulator;
        }
    }
}
