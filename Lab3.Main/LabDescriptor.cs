﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region P1

        public static Type I1 = typeof(ISterowanie);
        public static Type I2 = typeof(IZasilanie);
        public static Type I3 = typeof(IPamiec);

        public static Type Component = typeof(Roomba);

        public delegate object GetInstance(object component);

        public static GetInstance GetInstanceOfI1 = Roomba.DostepDoSterowania;
        public static GetInstance GetInstanceOfI2 = Roomba.DostepDoZasilania;
        public static GetInstance GetInstanceOfI3 = Roomba.DostepDoPamieci;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Roomba);
        public static Type MixinFor = typeof(Roomba);

        #endregion
    }
}
